import { Component, OnInit } from '@angular/core';
import { Movie } from '../movies/movie';

@Component({
  selector: 'app-komponent2',
  templateUrl: './komponent2.component.html',
  styleUrls: ['./komponent2.component.css']
})
export class Komponent2Component implements OnInit {

  constructor() { }

  // movies = ['Rouge one', 'Matrix', 'Terminator 2', 'Pulp Fiction'];

  movies = new Array<Movie>();

  ngOnInit() {
    this.movies.push(new Movie('Rouge One', 2016));
    this.movies.push(new Movie('Matrix', 1999));
    this.movies.push(new Movie('Lord of the rings', 2001));
    this.movies.push(new Movie('Titanic', 1997));
    this.movies.push(new Movie('Terminator 2', 1991));
    this.movies.push(new Movie('Pulp Fiction', 1994));
  }

  wyswietlNumer(index: number) {
    alert(index);
  }

  getOlderThan(year: number) {
    return this.movies.filter(m => m.year > year);
  }
}
