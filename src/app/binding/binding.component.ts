import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  constructor() { }

  propertyBinding = 'aaa';
  twoWayBinding = 'bbb';
  max = 5;

  ngOnInit() {
  }

  buttonClick(event) {
    console.dir(event); console.log('111');
    alert(this.propertyBinding + ' | ' + this.twoWayBinding);
  }

}
