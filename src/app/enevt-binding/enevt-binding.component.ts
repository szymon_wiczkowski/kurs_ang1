import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enevt-binding',
  templateUrl: './enevt-binding.component.html',
  styleUrls: ['./enevt-binding.component.css']
})
export class EnevtBindingComponent implements OnInit {

  isDisable = false;
  log: string;
  text1 = '';

  constructor() { }

  onBlur() {
    console.log('blur');
    this.isDisable = false;
  }

  onFocus() {
    console.log('focus');
    this.isDisable = true;
  }

  onMouseMove(event) {
    const msg = 'X: ' + event.clientX + ' | Y: ' + event.clientY;
    console.log(msg);
    this.log = msg;
  }

  onPaste(event) {
    console.log(event);
    alert('zakaz kopiowania!! ;)');
    this.text1 = '';
  }

  ngOnInit() {
  }

}
