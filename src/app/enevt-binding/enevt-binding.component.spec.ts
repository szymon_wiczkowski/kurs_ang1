import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnevtBindingComponent } from './enevt-binding.component';

describe('EnevtBindingComponent', () => {
  let component: EnevtBindingComponent;
  let fixture: ComponentFixture<EnevtBindingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnevtBindingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnevtBindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
