import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-komponent1',
  templateUrl: './komponent1.component.html',
  styleUrls: ['./komponent1.component.css']
})
export class Komponent1Component implements OnInit {

  czasTeraz = new Date();

  samochod = new Car('porsche', '911', 25.1234422);

  constructor() { }

  ngOnInit() {

  }

}

class Car {
  constructor(
    public marka: string,
    public model: string,
    public spalanie: number) { }
}
