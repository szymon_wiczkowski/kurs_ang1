import { Component, OnInit, Input } from '@angular/core';
import { Movie } from './movie';
import { LogiService } from '../logi.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  movies = new Array<Movie>();
  @Input()
  moviesLista = new Array<string>();
  // ['Breaking Bad', 'Mad Max', 'House of cards'];

  // isVisible = true;
  // isHidden = false;

  constructor(private logiService: LogiService) { }

  ngOnInit() {
    this.movies.push(new Movie('Rouge One', 2016));
    this.movies.push(new Movie('Matrix', 1999));
    this.movies.push(new Movie('Lord of the rings', 2001));
    this.movies.push(new Movie('Titanic', 1997));
    this.movies.push(new Movie('Terminator 2', 1991));
    this.movies.push(new Movie('Pulp Fiction', 1994));
    this.movies.push(new Movie('Star Wars IV', 1977));
  }

  // ngIfAkcja() {
  //   this.isVisible = !this.isVisible;
  // }

  // hiddenAkcja() {
  //   this.isHidden = !this.isHidden;
  // }



  addClick() {
    this.logiService.klikiButton('z movies komponent');
  }
}
