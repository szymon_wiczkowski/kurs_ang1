import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Kurs';
  text1 = 'jakis tam teskt';
  numere = 123;

  moviesZAppKomponent = ['Star Wars IV', 'Star Wars V', 'Star Wars VI'];

  addNewMovieToList(event) {
    console.log(event);
    this.moviesZAppKomponent.push(event);
  }

  getColor() {
    return 'green';
  }
}
