import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingZadanieComponent } from './binding-zadanie.component';

describe('BindingZadanieComponent', () => {
  let component: BindingZadanieComponent;
  let fixture: ComponentFixture<BindingZadanieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingZadanieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingZadanieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
