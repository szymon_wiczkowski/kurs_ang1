import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding-zadanie',
  templateUrl: './binding-zadanie.component.html',
  styleUrls: ['./binding-zadanie.component.css']
})
export class BindingZadanieComponent implements OnInit {
  inputMaxLength = 7;
  newText: string;
  someText = 'jaki text';
  imageUrl = 'https://www.cstatic-images.com/stock/1170x1170/59/img-1033855079-1518702927359.jpg';
  isBtnLock = false;

  constructor() { }

  ngOnInit() {
  }

  changeMe() {
    this.inputMaxLength = 10;
    this.someText = this.newText;
    this.newText = '';
    this.imageUrl = 'https://image.iol.co.za/image/1/process/620x349?source=https://inm-baobab-prod-eu-west-1.s3.amazonaws.com/public/inm/media/image/iol/2018/09/13/17049088/Porsche%20Cayenne%20Turbo%20front.jpg&operation=CROP&offset=542x685&resize=3796x2137';
    this.isBtnLock = true;
  }
}
