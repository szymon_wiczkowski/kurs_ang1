import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Komponent1Component } from './komponent1/komponent1.component';
import { Komponent2Component } from './komponent2/komponent2.component';
import { MoviesComponent } from './movies/movies.component';
import { BindingComponent } from './binding/binding.component';
import { FormsModule } from '@angular/forms';
import { BindingZadanieComponent } from './binding-zadanie/binding-zadanie.component';
import { EnevtBindingComponent } from './enevt-binding/enevt-binding.component';
import { MovieAddComponent } from './movie-add/movie-add.component';
import { MoviesColorsComponent } from './movies-colors/movies-colors.component';
import { LogiService } from './logi.service';

@NgModule({
  declarations: [
    AppComponent,
    Komponent1Component,
    Komponent2Component,
    MoviesComponent,
    BindingComponent,
    BindingZadanieComponent,
    EnevtBindingComponent,
    MovieAddComponent,
    MoviesColorsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [
    LogiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
