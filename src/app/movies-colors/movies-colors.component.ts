import { Component, OnInit } from '@angular/core';
import { Movie } from '../movies/movie';
import { LogiService } from '../logi.service';

@Component({
  selector: 'app-movies-colors',
  templateUrl: './movies-colors.component.html',
  styleUrls: ['./movies-colors.component.css']
})
export class MoviesColorsComponent implements OnInit {
  movies = new Array<Movie>();

  constructor(private logiService: LogiService) { }

  ngOnInit() {
    this.movies.push(new Movie('Rouge One', 2016));
    this.movies.push(new Movie('Matrix', 1999));
    this.movies.push(new Movie('Lord of the rings', 2001));
    this.movies.push(new Movie('Titanic', 1997));
    this.movies.push(new Movie('Terminator 2', 1991));
    this.movies.push(new Movie('Pulp Fiction', 1994));
    this.movies.push(new Movie('Star Wars IV', 1977));
  }

  getColor(year: number) {
    let color = '';
    if (year < 1989) {
      color = 'green';
    } else if (year < 1999) {
      color = 'orange';
    } else {
      color = 'red';
    }

    console.log('year: ' + year + '| color: ' + color);
    return color;
  }

  isLast(index: number) {
    return index == this.movies.length - 1;
  }

  addClick() {
    this.logiService.klikiButton('z movies.color komponent');
  }
}
