import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesColorsComponent } from './movies-colors.component';

describe('MoviesColorsComponent', () => {
  let component: MoviesColorsComponent;
  let fixture: ComponentFixture<MoviesColorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesColorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
