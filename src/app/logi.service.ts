import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogiService {
  private kliki = 0;

  constructor() { }

  klikiButton(source: string) {
    this.kliki += 1;
    console.log('kliki: ' + this.kliki + ' | source:' + source);
  }
}
