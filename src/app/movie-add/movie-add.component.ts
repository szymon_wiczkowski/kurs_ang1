import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  newMovieTitle: string;

  constructor() { }

  @Output()
  eventTask = new EventEmitter<string>();

  ngOnInit() {
  }

  addNewMovie() {
    this.eventTask.emit(this.newMovieTitle);
    this.newMovieTitle = '';
  }
}
